#include<windowsx.h>
#include<iostream>
#include<cstdio>
#include<time.h>
#include <Windows.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>
#include <fstream>
#include <clocale>
#include <string>

using namespace std;

struct type_matrix_point// ��� ��������� ������, �������� ���� � ��������� �� ��� �� ������, �� � ������� ���� type_array_cell
{
    int index; //��� ��������� � ������������ �������, ��������� ��� �� ������ (green_array, yellow_array)
    int color; //0 - �����, 1 - ������(���������), 2 - �����(�������)
};

struct type_array_cell//��� ������ �������, �������� ���������� �����.
{
    int x; 
    int y; 
};

const int sec = 1000; // ����������� ���������� � ������� (��� ������ � tps) 
const int matrix_size = 50; // ������ �������� �������
const int size_of_cell = 11;//������ ����� � ��������
const int max_figures = 100; //������������ ����������� �����
const int max_figures_points = 100; // ������������ ����������� ����� ������ �����
type_matrix_point matrix[matrix_size][matrix_size], new_matrix[matrix_size][matrix_size]; //�������� �������, � � ����� ��� ��������� ���������� ����
type_array_cell green_array[matrix_size*matrix_size], yellow_array[matrix_size*matrix_size]; // ������������� ������ ������ � ����� ������ (��� 2�� ���������)
type_array_cell new_green_array[matrix_size*matrix_size], new_yellow_array[matrix_size*matrix_size]; // ����� ��� ��������� ���������� ����
int alg_number, operating_mode, tps; // �������� ���������, ������� ��� �������� �  ����� (tps - ticks per second)
bool time_to_green;// ���������� ������������ ���������� �� ����� ��� ��������� ������ (��� ��� ������ ����� (������������ � �������) 1 ��� � 2 ����)
int number_green, number_yellow, new_number_green, new_number_yellow; //����������� ����� � ������ ������ (��� 2�� ���������) 
HDC hDC = GetDC( GetConsoleWindow( ) ); //(��� ����� ������� ������ �������� ���������, ����� �� ��������������)
HPEN White_Pen, Yellow_Pen, Green_Pen, Black_Pen; //����� ��� ��������� (����� �� ������� � �� ��������� ������)
int number_figures, green_figure, yellow_figure;//����������� �����, ������ ������� �����, ������ ������ �����
type_array_cell figures[max_figures][max_figures_points];//������ �����
int figures_numbers[max_figures]; //������ �������� ����������� �����, ��� ������ ������
string figures_names[max_figures];//����� �����



void Get_Options()//���������� ����� ����� � ����������
{
	ifstream options_file;
	options_file.open("options.txt", ios::in);
	options_file >> alg_number;
	options_file >> operating_mode;
    options_file >> tps;
}

void Get_Points()//���������� ��������� ����� �� ������ ��� ������������� ����������� �����
{
    int i;
    type_matrix_point start_value;
    type_array_cell start_point;
    ifstream yellow_file, green_file;
	yellow_file.open("yellow.txt", ios::in);
    green_file.open("green.txt", ios::in);

    //������
    start_value.color = 1;
    if(green_figure == -1)
    {
        green_file >> number_green;
        for(i = 0; i < number_green; i++)
        {
            green_file >> start_point.x >> start_point.y;
            start_value.index = i;
            matrix[start_point.x][start_point.y] = start_value;
            green_array[i] = start_point;
        }
    }
    else
    {
        number_green = figures_numbers[green_figure];
        for(i = 0; i < figures_numbers[green_figure]; i++)
        {
            start_point = figures[green_figure][i];
            start_value.index = i;
            matrix[start_point.x][start_point.y] = start_value;
            green_array[i] = start_point;
        }
    }

    //�����
    start_value.color = 2;
    if(yellow_figure == -1)
    {
        yellow_file >> number_yellow;
        for(i = 0; i < number_yellow; i++)
        {
            yellow_file >> start_point.x >> start_point.y;
            start_value.index = i;
            matrix[start_point.x][start_point.y] = start_value;
            yellow_array[i] = start_point;
        }
    }
    else
    {
        number_yellow = figures_numbers[yellow_figure];
        for(i = 0; i < figures_numbers[yellow_figure]; i++)
        {
            start_point = figures[yellow_figure][i];
            start_point.x += 25;
            start_point.y += 25;
            start_value.index = i;
            matrix[start_point.x][start_point.y] = start_value;
            yellow_array[i] = start_point;
        }
    }
    
}

void Crate_Place()//���������� ���������� ����
{  
    SelectObject( hDC, White_Pen );
   /* MoveToEx( hDC, 0, 85, NULL );
    LineTo( hDC, 200, 85 );
    MoveToEx( hDC, 100, 0, NULL );
    LineTo( hDC, 100, 170 );*/
    for(int i = 0; i <= matrix_size; i++)
    {
        MoveToEx( hDC, 0, i*(size_of_cell+1), NULL );
        LineTo( hDC, matrix_size*(size_of_cell+1), i*(size_of_cell+1));
        MoveToEx( hDC, i*(size_of_cell+1), 0, NULL );
        LineTo( hDC, i*(size_of_cell+1), matrix_size*(size_of_cell+1));
    }
}

void Update_Cell(int i, int j)//���������� ����������� ������
{
    int y = size_of_cell/2+1+i*(size_of_cell+1);
    int x = size_of_cell/2+1+j*(size_of_cell+1);
        if (matrix[i][j].color==0)
        {
            SelectObject( hDC, Black_Pen);
        }
        if (matrix[i][j].color==1)
        {
            SelectObject( hDC, Green_Pen );
        }
        if (matrix[i][j].color==2)
        {
            SelectObject( hDC, Yellow_Pen );
        }
        MoveToEx( hDC, x, y, NULL );
        LineTo( hDC, x, y);
}

void Set_Start_Data()//��������� ��������, ��������� ����� ������
{
    //��������� �������� ����������
    White_Pen = CreatePen( PS_SOLID, 1, RGB(255, 255, 255));
    Black_Pen = CreatePen( PS_SOLID, 10, RGB(0, 0, 0));
    Green_Pen = CreatePen( PS_SOLID, 10, RGB(0, 255, 0));
    Yellow_Pen = CreatePen( PS_SOLID, 10, RGB(255, 255, 0));
    number_green = 0;
    number_yellow = 0;
    new_number_green = 0;
    new_number_yellow = 0;
    time_to_green = true;
    alg_number = 0;
    operating_mode = 0;
    tps = 5;

    Get_Options();
    int i,j;
    type_matrix_point start_value;
    start_value.color = 0;
    start_value.index = 0;
    for(i = 0; i < matrix_size; i++)
        for(j = 0; j < matrix_size; j++)
            matrix[i][j] = start_value;

    Get_Points();

    for(i = 0; i < matrix_size; i++)
    {
        for(j = 0; j < matrix_size; j++)
        {
            new_matrix[i][j] = matrix[i][j];
            Update_Cell(i,j);
        }
    }

    for(i = 0; i < number_green; i++)
        new_green_array[i] = green_array[i];
    for(i = 0; i < number_yellow; i++)
        new_yellow_array[i] = yellow_array[i];
    new_number_green = number_green;
    new_number_yellow = number_yellow;
}

bool Show_Console_Cursor(bool bShow)//������� ����������� �������
{
    CONSOLE_CURSOR_INFO cci;
    HANDLE hStdOut;
    hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if(hStdOut == INVALID_HANDLE_VALUE)
        return false;
    if(!GetConsoleCursorInfo(hStdOut, &cci))
        return false;
    cci.bVisible = bShow;
    if(!SetConsoleCursorInfo(hStdOut,&cci))
        return false;
    return true;
}

void Update_Matrix_Cells()//���������� ������� � � �����������
{
    int i, j;
    for(i = 0; i < matrix_size; i++)
        for(j = 0; j < matrix_size; j++)
            if(matrix[i][j].color != new_matrix[i][j].color || matrix[i][j].index != new_matrix[i][j].index)
            {
                matrix[i][j] = new_matrix[i][j];
                Update_Cell(i,j);
            }
    for(i = 0; i < new_number_green; i++)
        green_array[i] = new_green_array[i];
    for(i = 0; i < new_number_yellow; i++)
        yellow_array[i] = new_yellow_array[i];
    number_green = new_number_green;
    number_yellow = new_number_yellow;

}

void Set_Console_Window()//�������� ����������� ����������� ����
{    
    //��������� ������� ����, ���-�� ����� � ��������
    system("mode con cols=75 lines=50");
    HWND wind_1 = GetForegroundWindow();
    COORD bufferSize = {matrix_size*(size_of_cell+1), matrix_size*(size_of_cell+1)};
    SetConsoleScreenBufferSize(wind_1, bufferSize);
    MoveWindow(wind_1,350,50, (matrix_size+5)*(size_of_cell+1),(matrix_size+7)*(size_of_cell+1),true);

    //��������� ����� ���� (������ ������ �������)
    SetWindowLongPtr(wind_1, GWL_STYLE, WS_CAPTION | WS_SYSMENU ); 

    //������� ������
    Show_Console_Cursor(false);

    //�������� ��������� ����
    SetConsoleTitle("Project Life");

    //���������� ����
    SetWindowRedraw(wind_1, true);
}

int Norm_Index(int k) // ��������� ������, �������� ������������� �������
{
    k %= matrix_size;
    if(k < 0)
        k += matrix_size;
    return k;
}

bool Check_Color_Cell(int i, int j, int color) // ��������� �� ����� � ������ ������ ������������ �����
{
    int number_of_color_cells = 0;
    //�������� ���� 8 ������ ������ �������
    if(matrix[Norm_Index(i-1)][Norm_Index(j-1)].color == color)
        number_of_color_cells++;
    if(matrix[Norm_Index(i)][Norm_Index(j-1)].color == color)
        number_of_color_cells++;
    if(matrix[Norm_Index(i+1)][Norm_Index(j-1)].color == color)
        number_of_color_cells++;
    if(matrix[Norm_Index(i-1)][Norm_Index(j)].color == color)
        number_of_color_cells++;
    if(matrix[Norm_Index(i+1)][Norm_Index(j)].color == color)
        number_of_color_cells++;
    if(matrix[Norm_Index(i-1)][Norm_Index(j+1)].color == color)
        number_of_color_cells++;
    if(matrix[Norm_Index(i)][Norm_Index(j+1)].color == color)
        number_of_color_cells++;
    if(matrix[Norm_Index(i+1)][Norm_Index(j+1)].color == color)
        number_of_color_cells++;

    if(matrix[i][j].color == color) 
    {
        //����������� �����
        if(number_of_color_cells >= 2 && number_of_color_cells <= 3)
            return true;
        else
            return false;
    }
    else 
    {
        // ����������
        if(number_of_color_cells == 3)
            return true;
        else
            return false;
    }
}

void Delete_Color_Cell(int i, int j) //�������� ����� ������
{
    type_array_cell tac;
    if(new_matrix[i][j].color == 1)
    {
        int k = new_matrix[i][j].index, x = new_green_array[new_number_green-1].x, y = new_green_array[new_number_green-1].y;
        tac = new_green_array[k];
        new_green_array[k] = new_green_array[new_number_green-1];
        new_green_array[new_number_green-1] = tac;
        new_matrix[x][y].index = k;
        matrix[x][y].index = k;
        new_matrix[i][j].color = 0;
        new_number_green--;
    }
    if(new_matrix[i][j].color == 2)
    {
        int k = new_matrix[i][j].index, x = new_yellow_array[new_number_yellow-1].x, y = new_yellow_array[new_number_yellow-1].y;
        tac = new_yellow_array[k];
        new_yellow_array[k] = new_yellow_array[new_number_yellow-1];
        new_yellow_array[new_number_yellow-1] = tac;
        new_matrix[x][y].index = k;
        matrix[x][y].index = k;
        new_matrix[i][j].color = 0;
        new_number_yellow--;
    }
}

void Algorithm_0()
{
	int i, j;
	for (i = 0; i < matrix_size; i++)
		for (j = 0; j < matrix_size; j++)
			if (matrix[i][j].color == 0)
			{
				if (time_to_green && new_matrix[i][j].color == 0 && Check_Color_Cell(i, j, 1))
					new_matrix[i][j].color = 1;
				if (new_matrix[i][j].color == 0 && Check_Color_Cell(i, j, 2))
					new_matrix[i][j].color = 2;
			}
			else
			{
				if (time_to_green)
				{
					if (Check_Color_Cell(i, j, 1))
						new_matrix[i][j].color = 1;
					else
						new_matrix[i][j].color = 0;
				}
				if (matrix[i][j].color == 2)
				{
					if (Check_Color_Cell(i, j, 2))
					{
						if (new_matrix[i][j].color != 1)
							new_matrix[i][j].color = 2;
					}
					else
					{
						if (new_matrix[i][j].color != 1)
							new_matrix[i][j].color = 0;
					}
				}
			}
}

void Algorithm_1()
{
    int i, x, y;
    //������
    if(time_to_green)
        for(i = 0; i < number_green; i++)
        {
            x = green_array[i].x;
            y = green_array[i].y;
            if(!Check_Color_Cell(x, y, 1))
                Delete_Color_Cell(x, y);
            for(int ki = -1; ki < 2; ki++)
            {
                for(int kj = -1; kj < 2; kj++)
                {
                    if(kj != 0 || kj != ki)
                    {
                        if(matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].color == 0 && new_matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].color == 0 && Check_Color_Cell(Norm_Index(x+ki), Norm_Index(y+kj), 1))
                        {
                            new_green_array[new_number_green].x = Norm_Index(x+ki);
                            new_green_array[new_number_green].y = Norm_Index(y+kj);
                            new_matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].color = 1;
                            new_matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].index = new_number_green;
                            new_number_green++;
                        }
                        else //������������� �����
                        if(matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].color != 1 && new_matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].color != 1 && Check_Color_Cell(Norm_Index(x+ki), Norm_Index(y+kj), 1))
                        {
                            Delete_Color_Cell(Norm_Index(x+ki),Norm_Index(y+kj));
                            new_green_array[new_number_green].x = Norm_Index(x+ki);
                            new_green_array[new_number_green].y = Norm_Index(y+kj);
                            new_matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].color = 1;
                            new_matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].index = new_number_green;
                            new_number_green++;
                        }
                    }
                }
            }
        }
    //�����
    for(i = 0; i < number_yellow; i++)
    {
        x = yellow_array[i].x;
        y = yellow_array[i].y;
        if(!Check_Color_Cell(x, y, 2) && new_matrix[x][y].color != 1)
            Delete_Color_Cell(x, y);
        for(int ki = -1; ki < 2; ki++)
        {
            for(int kj = -1; kj < 2; kj++)
            {
                if(kj != 0 || kj != ki)
                {
                    if(matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].color == 0 && new_matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].color == 0 && Check_Color_Cell(Norm_Index(x+ki), Norm_Index(y+kj), 2))
                    {
                        new_yellow_array[new_number_yellow].x = Norm_Index(x+ki);
                        new_yellow_array[new_number_yellow].y = Norm_Index(y+kj);
                        new_matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].color = 2;
                        new_matrix[Norm_Index(x+ki)][Norm_Index(y+kj)].index = new_number_yellow;
                        new_number_yellow++;
                    }
                }
            }
        }
    }
}

void Processing_Of_The_Next_Step() //��������� ��� ��� ���� ��������, � ����������� �� ��������
{
    if(alg_number == 1)
        Algorithm_1();
    else
        Algorithm_0();
}

void Print_Figures_List() //����� ������ ����� �� �����
{
    int i;
    for(i = 0; i < number_figures; i++)
        cout << i << " - " << figures_names[i] << endl;
}

void Users_Menu() // ���������������� ��������� � ������ ���������
{
    setlocale(LC_CTYPE, "rus");
    int a, b;
    while(true)
    {
        cout << "�������� ���� �� ������������ �������� ��� ������� �����:"<<endl;
        cout << "1 - ������� ����� �� �����"<<endl;
        cout << "2 - ������� ���� �� ������������ �����"<<endl;
        cin >> a;
        if (a == 1)
        {
           green_figure = -1;
           break;
        }
        if (a == 2)
        {
            system("cls");
            cout << "�������� ���� �� ������������ ����� ��� ������� �����:" << endl;
            Print_Figures_List();
            cin >> b;
            if(b >= 0  && b < number_figures)
            {
                green_figure = b;
                break;
            }
            else
                cout << "error" << endl;
        }
        if(a != 1 && a != 2)
            cout << "error" << endl;
        system("pause");
        system("cls");
    }
    system("cls");
    while(true)
    {
        cout << "�������� ���� �� ������������ �������� ��� ������ �����:"<<endl;
        cout << "1 - ������� ����� �� �����"<<endl;
        cout << "2 - ������� ���� �� ������������ �����"<<endl;
        cin >> a;
        if (a == 1)
        {
           yellow_figure = -1;
           break;
        }
        if (a == 2)
        {
            system("cls");
            cout << "�������� ���� �� ������������ ����� ��� ������ �����:" << endl;
            Print_Figures_List();
            cin >> b;
            if(b >= 0  && b < number_figures)
            {
                yellow_figure = b;
                break;
            }
            else
                cout << "error" << endl;
        }
        if(a != 1 && a != 2)
            cout << "error" << endl;
        system("pause");
        system("cls");
    }
    system("pause");
    system("cls");
}

void Get_Figures()// ���������� ������
{
    ifstream figure_file;
    figure_file.open("figure.txt", ios::in);
    int i,j;
    figure_file >> number_figures;
    for(i = 0; i < number_figures; i++)
    {
        figure_file >> figures_names[i] >> figures_numbers[i];
        for(j = 0; j < figures_numbers[i]; j++)
            figure_file >> figures[i][j].x >> figures[i][j].y;
    }
}

int main()
{  
    Get_Figures();
    Users_Menu();
    Set_Console_Window();
    Set_Start_Data();
    Crate_Place();   
    //����������, ��� ������ � tps
    int time = clock(), current_time;
    while (true)
    {
        //������ ������������ ������ ������
        if(operating_mode == 1)
            getch();
        current_time = clock();
        if(current_time > time + sec/tps || current_time < time)
        {
            hDC = GetDC( GetConsoleWindow( ) );

            time = current_time;
            Processing_Of_The_Next_Step();
            Update_Matrix_Cells();

            //������������� ��� ������ ������ (��������� 1 ��� � 2 ����)
            if(time_to_green)
                time_to_green = false;
            else
                time_to_green = true;
        }
    }
    return 0;
}